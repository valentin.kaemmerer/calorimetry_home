from functions import m_json
from functions import m_pck

path = ""

#Versuch (Messung): Wärmekapazität eines Behälters
print("Erster Versuch (Messung):")
#Metadaten aus der setup json ausgeben
metadata = m_json.get_metadata_from_setup("/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json")

#metadata um serials der Sensoren erweitern
metadata = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata)

#Die Messdaten aufnehmen
data = m_pck.get_meas_data_calorimetry(metadata)

#Messdaten in einer H5-Datei speichern
m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/heat_capacity_measurement_data","/home/pi/calorimetry_home/datasheets")

#verwendete jsons archivieren
m_json.archiv_json("/home/pi/calorimetry_home/datasheets","/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json","/home/pi/calorimetry_home/data/heat_capacity_measurement_data")



#Versuch (Messung): thermische Verluste
print("Zweiter Versuch (Messung):")

#Metadaten aus der setup json ausgeben
metadata = m_json.get_metadata_from_setup("/home/pi/calorimetry_home/datasheets/setup_newton.json")

#metadata um serials der Sensoren erweitern
metadata = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets", metadata)

#Die Messdaten aufnehmen
data = m_pck.get_meas_data_calorimetry(metadata)

#Messdaten in einer H5-Datei speichern
m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/newton_measurement_data","/home/pi/calorimetry_home/datasheets")

#verwendete jsons archivieren
m_json.archiv_json("/home/pi/calorimetry_home/datasheets","/home/pi/calorimetry_home/datasheets/setup_newton.json","/home/pi/calorimetry_home/data/newton_measurement_data")